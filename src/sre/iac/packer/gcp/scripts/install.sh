#!/usr/bin/env bash

echo "Installing dependencies..."

sudo apt-get update

sudo apt-get install -y curl redis-server git python3.6
