module "container_stack" {
  source = "./container_stack"

  container_count = 4
  image_name = "httpd"
}
