
locals {
  container_count = 3
}

# resource <resource_type> <resource_name(ID)>
resource "docker_image" "nginx" {
  name         = var.image_name
  keep_locally = false
}

# dependencies
resource "docker_container" "nginx" {

  count = local.container_count
  image = docker_image.nginx.name
  name  = "terraform-tutorial-${count.index}"

  ports {
    internal = 80
    external = "808${count.index}"
  }

  labels {
    label = "environment"
    value = "development"
  }

  labels {
    label = "image-txt"
    value = "The image is ${docker_image.nginx.name}"
  }

}


