# Micro Backend

+ [x] Create MemoryStore Redis
+ [x] Create VPC access for serverless
+ [x] Convert our app frontend, backend to gcloud builds
+ [x] Deploy both apps to cloud run
