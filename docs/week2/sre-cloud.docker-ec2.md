# Compose


"Compose is a tool for defining and running multi-container Docker applications. With Compose, you use a YAML file to configure your application’s services. Then, with a single command, you create and start all the services from your configuration. To learn more about all the features of Compose."



## Containers on EC2 (primer)

Install Docker 🐳 and docker-compose

```
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh


sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

sudo usermod -aG docker $USER


```

### Start the application


Using docker-compose 🔥

```
# render the docker-compose.yml file
docker-compose config

# build the images
docker-compose build

export SUBDOMAIN=<your-Public-IP>
# start the containers
docker-compose up

# go to your browser at http://<your-Public-IP>.traefik.me
```


Using docker cmds 👎


---


