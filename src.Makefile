### SRC ###
app:
	docker-compose -f ./docker-compose.yml up -d --build app

redis:
	docker-compose -f ./docker-compose.yml up -d --build redis

micro:
	docker-compose -f ./docker-compose.yml up -d --build micro

all:
	docker-compose -f ./docker-compose.yml up -d --build

rm:
	docker-compose -f ./docker-compose.yml down
